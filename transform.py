import glob
import re


def main():
    with open("_output_1", "w") as o1,\
            open("_output_2", "w") as o2,\
            open("_output_3", "w") as o3:
        for f_name in sorted(glob.glob("input/*.txt")):
            with open(f_name, "r", encoding="cp1250") as i:
                for line in i:
                    transform(line, f_name, o1, o2, o3)


def transform(line, f_name, o1, o2, o3):
    o1.write(
        "./{0}{1}".format(
            str(f_name).rstrip(".txt"),
            re.sub("\\s{2,}", "|", line, count=3)
            .replace("|X ", "|", 1).replace(" ", "|", 1)
        )
    ) if re.search("^\\s+[A-Z0-9_]+\\s+[X]?\\s[A-Z0-9]+\\s+\\d+.*", line)\
        else o3.write(line)

    if re.search("^\\s+(Krátký popis:|Počet polí:)", line):
        o2.write("./{0}|{1}".format(str(f_name).rstrip(".txt"), line))


if __name__ == "__main__":
    main()
